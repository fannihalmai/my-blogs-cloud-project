# my-films-cloud-project

## Project description
This sample project aims at showing how to deploy two dockerized services via Kubernetes.
The frontend is written in React while the backend is pure javascript.

## To run the project locally

```
cd api
npm i
npm run start

cd .. 
cd mybooks
npm i
npm run start
```

## To dockerize the services
```
docker-compose up

```

## Deploy with Kubernetes