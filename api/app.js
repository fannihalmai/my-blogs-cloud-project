const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())

app.get('/', (req, res) => {
  res.json([
    {
      "id":"1",
      "title":"The stranger"
    },
    {
      "id":"2",
      "title":"Madame Bovary"
    },
    {
      "id":"3",
      "title":"Red and Black"
    }
  ])
})

app.listen(4000, () => {
  console.log('listening for requests on port 4000')
})